# Références

- Pour calendrier : 
    * https://github.com/tattali/CalendarBundle
    * https://github.com/tattali/CalendarBundle/blob/master/src/Resources/doc/doctrine-crud.md#full-subscriber

# Tuto PostgreSQL

# Symfony

## Authentification & Roles:

- https://symfony.com/doc/current/security.html#add-code-to-deny-access

## Bonnes pratiques

- https://symfony.com/doc/current/best_practices.html#use-environment-variables-for-infrastructure-configuration

## Héritage

- https://www.youtube.com/watch?v=hNAMEWYCkAM
- https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/reference/inheritance-mapping.html

## Ajout de données par défaut (Fixtures)

- https://symfony.com/doc/current/testing/database.html#doctrine-fixtures
- https://symfony.com/doc/current/testing/database.html#dummy-data-fixtures

## Insert ManyToMany (inversedBy/mappedBy)

- https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/reference/unitofwork-associations.html
