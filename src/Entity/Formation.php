<?php

namespace App\Entity;

use App\Repository\FormationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FormationRepository::class)
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Positive
     */
    private $duree_annees;


    /**
     * @ORM\OneToMany(targetEntity=Groupe::class, mappedBy="formation", orphanRemoval=true)
     */
    private $groupes;

    /**
     * @ORM\OneToOne(targetEntity=Professeur::class, inversedBy="formation_responsable", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $responsable;

    public function __construct()
    {
        $this->groupes = new ArrayCollection();
        $this->duree_annees = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getDureeAnnees(): ?int
    {
        return $this->duree_annees;
    }

    public function setDureeAnnees(int $duree_annees): self
    {
        $this->duree_annees = $duree_annees;

        return $this;
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): self
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
            $groupe->setFormation($this);
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): self
    {
        if ($this->groupes->contains($groupe)) {
            $this->groupes->removeElement($groupe);
            // set the owning side to null (unless already changed)
            if ($groupe->getFormation() === $this) {
                $groupe->setFormation(null);
            }
        }

        return $this;
    }

    public function getResponsable(): ?Professeur
    {
        return $this->responsable;
    }

    public function setResponsable(Professeur $responsable): self
    {
        $this->responsable = $responsable;

        return $this;
    }

    public function __toString()
    {
        return strval($this->getIntitule());
    }
}
