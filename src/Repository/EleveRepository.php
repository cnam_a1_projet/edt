<?php

namespace App\Repository;

use App\Entity\Cours;
use App\Entity\Eleve;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use function get_class;

/**
 * @method Eleve|null find($id, $lockMode = null, $lockVersion = null)
 * @method Eleve|null findOneBy(array $criteria, array $orderBy = null)
 * @method Eleve[]    findAll()
 * @method Eleve[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EleveRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Eleve::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof Eleve) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function getElevesSansAbsences(Cours $cours)
    {
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();
        $eleves_deja_absents = $qb->select("e")
            ->from("App:Eleve", "e")
            ->leftJoin("e.absences", "a")
            ->where("a.cours = :cours")
            ->setParameter("cours", $cours)
            ->getQuery()
            ->getResult();

        $qb = $entityManager->createQueryBuilder();
        $qb->select("e")
            ->from("App:Eleve", "e")
            ->where($qb->expr()->eq("e.groupe", ":groupe"))
            ->setParameter("groupe", $cours->getGroupe());
        if (count($eleves_deja_absents) != 0) {
            $qb->andWhere($qb->expr()->notIn("e", ":eleves_deja_absents"))
                ->setParameter("eleves_deja_absents", $eleves_deja_absents);
        }

        return $qb->getQuery()->getResult();
    }

    public function getElevesSansNotes(\App\Entity\Examen $examen)
    {

        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();
        $eleves_deja_notes = $qb->select("e")
            ->from("App:Eleve", "e")
            ->leftJoin("e.notes", "n")
            ->where("n.examen = :examen")
            ->setParameter("examen", $examen)
            ->getQuery()
            ->getResult();

        $qb = $entityManager->createQueryBuilder();
        $qb->select("e")
            ->from("App:Eleve", "e")
            ->where($qb->expr()->eq("e.groupe", ":groupe"))
            ->setParameter("groupe", $examen->getGroupe());
        if (count($eleves_deja_notes) != 0) {
            $qb->andWhere($qb->expr()->notIn("e", ":eleves_deja_notes"))
                ->setParameter("eleves_deja_notes", $eleves_deja_notes);
        }

        return $qb->getQuery()->getResult();

    }


    // /**
    //  * @return Eleve[] Returns an array of Eleve objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Eleve
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
